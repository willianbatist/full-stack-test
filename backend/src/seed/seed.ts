import { PrismaClient } from '@prisma/client';
import * as fs from 'fs';

const prisma = new PrismaClient();

const seed = async () => {
  try {
    const moviesData = JSON.parse(fs.readFileSync('src/assets/movies.json', 'utf-8'));

    for (const movie of moviesData) {
      await prisma.movie.create({
        data: {
          id: movie.id,
          original_language: movie.original_language,
          original_title: movie.original_title,
          overview: movie.overview,
          popularity: movie.popularity,
          poster_path: movie.poster_path,
          release_date: movie.release_date,
          title: movie.title,
          video: movie.video,
          vote_average: movie.vote_average,
          vote_count: movie.vote_count,
        },
      });
    }

    console.log('Movies seeded successfully.');
  } catch (error) {
    console.error('Error seeding movies:', error);
  } finally {
    await prisma.$disconnect();
  }
};

seed();
