import { Injectable, ConflictException, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { Prisma } from '@prisma/client';
import * as bcrypt from 'bcrypt';
import { CreateUserDto } from './dto/create-user.dto';
import { UserRepository } from 'src/repositories/user-repository';
import { User } from './entities/user.entity';
import { UpdateUserDto } from './dto/update-user.dto';

@Injectable()
export class UserService {
  constructor(private userRepository: UserRepository) {}

  async create(createUserDto: CreateUserDto): Promise<User> {
    const data: Prisma.UserCreateInput = {
      ...createUserDto,
      password: await bcrypt.hash(createUserDto.password, 10),
    };

    try {
      const createdUser = await this.userRepository.create(data);

      return {
        ...createdUser,
        password: undefined,
      };
    } catch (error) {
      if (error instanceof Prisma.PrismaClientKnownRequestError) {
        if (error.code === 'P2002') {
          throw new ConflictException('Email is already in use');
        }
      }
      throw new InternalServerErrorException('Internal server error');
    }
  }

  async findByEmail(email: string) {
    return this.userRepository.findByEmail(email);
  }

  async update(data: UpdateUserDto) {
    try {
      const user = await this.findByEmail(data.email);
      if (!user) {
        throw new NotFoundException('User not found');
      }
      return await this.userRepository.update({
        email: data.email,
        password: await bcrypt.hash(data.password, 10)
      });
    } catch (error) {
      throw new InternalServerErrorException('Internal server error');
    }
  }

  async delete(id: string, email: string) {
    try {
      const user = await this.findByEmail(email);
      if (!user) {
        throw new NotFoundException('User not found');
      }
      if (user.id !== id) {
        throw new NotFoundException('Id not found');
      }
      return await this.userRepository.delete(id);
    } catch (error) {
      throw new InternalServerErrorException('Internal server error');
    }
  }
}
