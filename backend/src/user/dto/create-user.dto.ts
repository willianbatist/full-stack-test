import { User } from '../entities/user.entity';
import { IsEmail, IsString, MaxLength, MinLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateUserDto extends User {
  @ApiProperty({
    example: 'exemple@email.com',
    description: `O e-mail é necessário apra o login.`,
  })
  @IsEmail()
  email: string;

  @ApiProperty({
    minLength: 4,
    maxLength: 20,
    example: 'P@ssw0rd',
    description: `Senha para acesso, mesma deve conter pelo menos um caracteres especiais, maiúsculo e numérico`,
  })
  @MinLength(4)
  @MaxLength(20)
  @IsString()
  password: string;

  @ApiProperty({
    example: 'Willian Alves Batista',
  })
  @IsString()
  name: string;
}
