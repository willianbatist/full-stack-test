import { UpdateMovieDto } from "src/movie/dto/update-movie.dto";
import { Movie } from "src/movie/entities/movie.entity";

export abstract class MovieRepository {
  abstract findByIdMovie(id: number): Promise<Movie | null>;
  abstract findAllMovies(): Promise<Movie[] | null>;
  abstract findAllMoviesPaginated(page: number, limit: number): Promise<{ movies: Movie[], count: number }>;
  abstract createMovie(data: Omit<Movie, 'id'>): Promise<Movie>;
  abstract updateMovie(id: number, data: UpdateMovieDto): Promise<Movie>;
  abstract deleteMovie(id: number): Promise<Movie>;
}