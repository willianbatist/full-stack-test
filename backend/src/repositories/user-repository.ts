import { UpdateUserDto } from 'src/user/dto/update-user.dto';
import { User } from 'src/user/entities/user.entity';

export abstract class UserRepository {
  abstract create(data: User): Promise<User>;
  abstract findByEmail(email: string): Promise<User | null>;
  abstract update(data: UpdateUserDto): Promise<User | null>;
  abstract delete(id: String):Promise<unknown>;
}
