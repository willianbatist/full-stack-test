import { User } from 'src/user/entities/user.entity';
import { CreateUserDto } from 'src/user/dto/create-user.dto';
import { Injectable } from '@nestjs/common';
import { UserRepository } from '../user-repository';
import { PrismaService } from 'src/database/prisma.service';
import { UpdateUserDto } from 'src/user/dto/update-user.dto';

@Injectable()
export class PrismaUserRepository implements UserRepository {
  constructor(private prisma: PrismaService) {}

  async update(data: UpdateUserDto): Promise<User> {
    return await this.prisma.user.update(
      {
        where: { email: data.email },
        data: { password: data.password }
      }
    );
  }

  async delete(id: string): Promise<unknown> {
    return await this.prisma.user.delete({ where: { id } });
  }

  async findByEmail(email: string): Promise<User> {
    return await this.prisma.user.findUnique({ where: { email } });
  }

  async create(data: CreateUserDto): Promise<User> {
    const user = await this.prisma.user.create({ data });
    return user;
  }
}