import { Injectable } from '@nestjs/common';
import { MovieRepository } from '../movie-repository';
import { PrismaService } from 'src/database/prisma.service';
import { Movie } from 'src/movie/entities/movie.entity';
import { CreateMovieDto } from 'src/movie/dto/create-movie.dto';
import { UpdateMovieDto } from 'src/movie/dto/update-movie.dto';

@Injectable()
export class PrismaMovieRepository implements MovieRepository {
  constructor(private prisma: PrismaService) {}

  async findAllMoviesPaginated(
    page: number,
    limit: number,
  ): Promise<{ movies: Movie[]; count: number }> {
    const skip = (page - 1) * limit;
    const movies = await this.prisma.movie.findMany({
      skip,
      take: limit,
    });
    const count = await this.prisma.movie.count();
    return { movies, count };
  }

  async findByIdMovie(id: number): Promise<Movie> {
    return await this.prisma.movie.findUnique({ where: { id } });
  }

  async findAllMovies(): Promise<Movie[]> {
    return await this.prisma.movie.findMany();
  }

  async createMovie(data: CreateMovieDto): Promise<Movie> {
    return await this.prisma.movie.create({ data });
  }

  async updateMovie(id: number, data: UpdateMovieDto): Promise<Movie> {
    return await this.prisma.movie.update({
      where: { id },
      data,
    });
  }

  async deleteMovie(id: number): Promise<Movie> {
    return await this.prisma.movie.delete({
      where: { id },
    });
  }
}
