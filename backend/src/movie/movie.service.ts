import {
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { MovieRepository } from 'src/repositories/movie-repository';
import { Movie } from './entities/movie.entity';
import { CreateMovieDto } from './dto/create-movie.dto';
import { UpdateMovieDto } from './dto/update-movie.dto';

@Injectable()
export class MovieService {
  constructor(private movieRepository: MovieRepository) {}

  async findByIdMovie(id: number): Promise<Movie> {
    try {
      const movie = await this.movieRepository.findByIdMovie(id);
      if (!movie) {
        throw new NotFoundException('Movie not found');
      }
      return movie;
    } catch (error) {
      throw new InternalServerErrorException('Internal server error');
    }
  }

  async findAllMovies(
    page?: number,
    limit?: number,
  ): Promise<{ movies: Movie[]; count: number }> {
    try {
      if (page && limit) {
        return await this.movieRepository.findAllMoviesPaginated(page, limit);
      } else {
        const movies = await this.movieRepository.findAllMovies();
        return { movies, count: movies.length };
      }
    } catch (error) {
      throw new InternalServerErrorException('Internal server error');
    }
  }

  async createMovie(data: CreateMovieDto): Promise<Movie> {
    try {
      return await this.movieRepository.createMovie(data);
    } catch (error) {
      throw new InternalServerErrorException('Internal server error');
    }
  }

  async updateMovie(id: number, data: UpdateMovieDto): Promise<Movie> {
    try {
      const movie = await this.movieRepository.findByIdMovie(id);
      if (!movie) {
        throw new NotFoundException('Movie not found');
      }
      return await this.movieRepository.updateMovie(id, data);
    } catch (error) {
      throw new InternalServerErrorException('Internal server error');
    }
  }

  async deleteMovie(id: number): Promise<Movie> {
    try {
      const movie = await this.movieRepository.findByIdMovie(id);
      if (!movie) {
        throw new NotFoundException('Movie not found');
      }
      return await this.movieRepository.deleteMovie(id);
    } catch (error) {
      throw new InternalServerErrorException('Internal server error');
    }
  }
}
