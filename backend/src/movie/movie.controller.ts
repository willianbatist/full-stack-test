import { Body, Controller, Delete, Get, Param, Post, Put, Query } from '@nestjs/common';
import { MovieService } from './movie.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { CreateMovieDto } from './dto/create-movie.dto';
import { UpdateMovieDto } from './dto/update-movie.dto';

@ApiTags('Movie')
@Controller()
export class MovieController {
  constructor(private readonly movieService: MovieService) {}

  @Get('movie/:id')
  @ApiBearerAuth('access-token')
  async findByIdMovie(@Param('id') id: number) {
    return await this.movieService.findByIdMovie(id);
  }

  @Get('movies')
  @ApiBearerAuth('access-token')
  async findAllMovies(
    @Query('page') page: number,
    @Query('limit') limit: number,
  ) {
    if (page && limit) {
      return await this.movieService.findAllMovies(+page, +limit);
    } else {
      return await this.movieService.findAllMovies();
    }
  }

  @Post('movie')
  @ApiBearerAuth('access-token')
  async createMovie(@Body() data: CreateMovieDto) {
    return await this.movieService.createMovie(data);
  }

  @Put('movie/:id')
  @ApiBearerAuth('access-token')
  async updateMovie(@Param('id') id: number, @Body() data: UpdateMovieDto) {
    return await this.movieService.updateMovie(id, data);
  }

  @Delete('movie/:id')
  @ApiBearerAuth('access-token')
  async deleteMovie(@Param('id') id: number) {
    return await this.movieService.deleteMovie(id);
  }
}
