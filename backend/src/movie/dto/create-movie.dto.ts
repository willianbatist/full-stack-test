import { IsString, IsNumber, IsBoolean, IsInt } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateMovieDto {
  @ApiProperty({
    example: 'en',
  })
  @IsString()
  original_language: string;

  @ApiProperty({
    example: 'Culpa mía',
  })
  @IsString()
  original_title: string;

  @ApiProperty({
    example: 'Noah must leave her city, boyfriend...',
  })
  @IsString()
  overview: string;

  @ApiProperty({
    example: '417.273',
  })
  @IsNumber()
  popularity: number;

  @ApiProperty({
    example: '/w46Vw536HwNnEzOa7J24YH9DPRS.jpg',
  })
  @IsString()
  poster_path: string;

  @ApiProperty({
    example: '2023-06-08',
  })
  @IsString()
  release_date: string;

  @ApiProperty({
    example: 'My Fault',
  })
  @IsString()
  title: string;

  @ApiProperty({
    example: 'false',
  })
  @IsBoolean()
  video: boolean;

  @ApiProperty({
    example: '7.964',
  })
  @IsNumber()
  vote_average: number;

  @ApiProperty({
    example: '2598',
  })
  @IsInt()
  vote_count: number;
}
