import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsString, IsNumber, IsBoolean, IsInt, IsOptional } from 'class-validator';

export class UpdateMovieDto {
  @ApiPropertyOptional({
    example: 'en',
  })
  @IsOptional()
  @IsString()
  original_language?: string;

  @ApiPropertyOptional({
    example: 'Culpa mía',
  })
  @IsOptional()
  @IsString()
  original_title?: string;

  @ApiPropertyOptional({
    example: 'Noah must leave her city, boyfriend...',
  })
  @IsOptional()
  @IsString()
  overview?: string;

  @ApiPropertyOptional({
    example: '417.273',
  })
  @IsOptional()
  @IsNumber({ allowInfinity: false, allowNaN: false }, { each: false })
  popularity?: number;

  @ApiPropertyOptional({
    example: '/w46Vw536HwNnEzOa7J24YH9DPRS.jpg',
  })
  @IsOptional()
  @IsString()
  poster_path?: string;

  @ApiPropertyOptional({
    example: '2023-06-08',
  })
  @IsOptional()
  @IsString()
  release_date?: string;

  @ApiPropertyOptional({
    example: 'My Fault',
  })
  @IsOptional()
  @IsString()
  title?: string;

  @ApiPropertyOptional({
    example: 'false',
  })
  @IsOptional()
  @IsBoolean()
  video?: boolean;

  @ApiPropertyOptional({
    example: '7.964',
  })
  @IsOptional()
  @IsNumber({ allowInfinity: false, allowNaN: false }, { each: false })
  vote_average?: number;

  @ApiPropertyOptional({
    example: '2598',
  })
  @IsOptional()
  @IsInt()
  vote_count?: number;
}
