import { Module } from '@nestjs/common';
import { MovieService } from './movie.service';
import { MovieController } from './movie.controller';
import { PrismaService } from 'src/database/prisma.service';
import { MovieRepository } from 'src/repositories/movie-repository';
import { PrismaMovieRepository } from 'src/repositories/prisma/prisma-movie-repository';

@Module({
  controllers: [MovieController],
  providers: [
    MovieService,
    PrismaService,
    {
      provide: MovieRepository,
      useClass: PrismaMovieRepository,
    },
  ],
})
export class MovieModule {}
