export class Movie {
  id?: number;
  original_language: String;
  original_title: String;
  overview: String;
  popularity: number;
  poster_path: String;
  release_date: String;
  title: String;
  video: Boolean;
  vote_average: number;
  vote_count: number;
}
