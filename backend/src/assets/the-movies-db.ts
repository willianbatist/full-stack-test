import axios from 'axios';
import * as fs from 'fs';
import * as dotenv from 'dotenv';

dotenv.config();

const apiKey = process.env.API_KEY;
const startDate = '2023-01-01';
const endDate = '2024-01-31';

const fetchMovies = async () => {
  if (!apiKey) {
    console.error('API_KEY not defined in .env file');
    return;
  }

  const baseUrl = `https://api.themoviedb.org/3/discover/movie?api_key=${apiKey}&primary_release_date.gte=${startDate}&primary_release_date.lte=${endDate}`;

  let allMovies: any[] = [];

  for (let page = 1; page <= 3; page++) {
    try {
      const response = await axios.get(`${baseUrl}&page=${page}`);
      allMovies = allMovies.concat(response.data.results);
    } catch (error) {
      console.error(`Error when searching the page ${page}:`, error);
    }
  }

  fs.writeFile('src/assets/movies.json', JSON.stringify(allMovies, null, 2), (err) => {
    if (err) {
      console.error('Error writing file:', err);
    } else {
      console.log('Movie data saved in movies.json');
    }
  });
};

fetchMovies();
