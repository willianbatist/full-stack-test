import { Module } from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { PrismaService } from './database/prisma.service';
import { JwtAuthGuard } from './auth/guards/jwt-auth.guard';
import { MovieModule } from './movie/movie.module';

@Module({
  imports: [UserModule, AuthModule, MovieModule],
  controllers: [],
  providers: [
    PrismaService,
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    },
  ],
})
export class AppModule {}
