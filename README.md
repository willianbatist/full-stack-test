## Teste Desenvolvedor Full Stack - Willian Alves Batista

O teste consiste em uma aplicação com Backend NodeJs que expõe uma API REST de um CRUD de usuários e filmes e uma aplicação web contendo uma interface(React/Next.JS) para login e acesso a dados de uma API externa.


#### Tecnologias utilizadas:

  - TypeScript;
  - Node.js;
  - Nest.js;
  - Swagger;
  - Axios;
  - Jwt;
  - Class-validator;
  - Bcrypt;
  - Postgresql;
  - Docker;
  - Restful;
  - eslint;
  - vscode;
  - React.js;
  - Next.js;
  - Styled-Components;
  - Chakra-ui;


## Inciando o projeto

:warning: **É necessário ter o docker-compose instalado na maquina.**

Para iniciar o projeto, basta baixar ou clonar este repositório.

Acesse a raiz do projeto, abra o terminal, em seguida digite:

    docker-compose -f docker-compose.yml up -d

Tudo certo vai está disponível em:

**Front-end**
http://localhost:3000/

**Back-end**
http://localhost:4000/


## Front-end

O front-end foi desenvolvido em React/Next e apresenta os seguintes requisitos:
•  Interface de login
•  Feedbacks de usuário ou senha incorreta
•  Listagem dos dados de filmes
•  Paginação dos dados
•  Listagem dos dados do Usuário


## Back-end

:warning: **Documentação Swagger da API fica disponível http://localhost:4000/api**

API REST de um CRUD de usuários e filmes com todos os endpoints de consulta de dados com autenticação por Token.

