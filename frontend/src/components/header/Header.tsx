import { HeaderStyled } from "./header.style";
import Image from "next/image";
import iconUser from "../../../public/images/icon-user.png";

interface HeaderProps {
  onOpen: () => void;
}

const Header: React.FC<HeaderProps> = ({ onOpen }) => {
  return (
    <HeaderStyled>
      <button onClick={onOpen}>
        <Image src={iconUser} alt="Logo" width={50} height={25} />
      </button>
    </HeaderStyled>
  );
};

export default Header;
