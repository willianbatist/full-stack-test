'use client'
import styled from 'styled-components';

export const HeaderStyled = styled.header`
  width: 100vw;
  height: 80px;
  background-color: rgb(30, 30, 34);
  display: flex;
  align-content: center;
  justify-content: flex-end;
  
  button {
    margin-right: 50px;
  }
`;