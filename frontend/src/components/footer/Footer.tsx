"use client";
import React from "react";
import { FooterStyled } from "./footer.styles";

export default function Footer() {
  return (
    <FooterStyled>
      <p>
        Developed by <span>Willian Alves Batista</span>.
      </p>
    </FooterStyled>
  );
}
