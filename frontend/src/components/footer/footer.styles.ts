'use client'
import styled from 'styled-components';


export const FooterStyled = styled.footer`
  width: 100vw;
  height: 50px;
  text-align: center;
  color: white;
  background-color: rgb(30, 30, 34);
  display: flex;
  justify-content: center;
  align-items: center;

  span {
    color: #E96744;
  }
`