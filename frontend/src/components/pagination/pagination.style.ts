'use client';
import styled from 'styled-components';

export const PaginationStyled = styled.div`
  width: 300px;
  display: flex;
  justify-content: space-evenly;
  color: white;
  font-weight: bold;
  font-size: 20px;

  margin-top: 20px;
  margin-bottom: 40px;

  button:hover {
    color: #E96744;
  }
`;