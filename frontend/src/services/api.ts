import axios from 'axios';

const API_URL = 'http://localhost:4000';

export const login = async (email: string, password: string) => {
  try {
    const response = await axios.post(`${API_URL}/login`, {
      email,
      password,
    });
    return response.data;
  } catch (error) {
    if (axios.isAxiosError(error)) {
      if (error.response?.status === 401) {
        throw new Error('Unauthorized');
      }
    }
    throw new Error('failed');
  }
};

export const createUser = async (name: string, email: string, password: string) => {
  try {
    const response = await axios.post(`${API_URL}/user`, {
      name,
      email,
      password,
    });
    return response.data;
  } catch (error) {
    if (axios.isAxiosError(error)) {
      if (error.response?.status === 409) {
        throw new Error('Conflict');
      }
    }
    throw new Error('failed');
  }
};


export const fetchUser = async (token: string) => {
  try {
    const response = await axios.get(
      `${API_URL}/user`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    return response.data;
  } catch (error) {
    if (axios.isAxiosError(error)) {
      if (error.response?.status === 401) {
        throw new Error('Unauthorized');
      }
    }
    throw new Error('failed');
  }
};