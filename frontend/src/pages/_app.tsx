import { AppProps } from "next/app";
import Head from "next/head";
import GlobalStyles from "../styles/global";
import AppProvider from "@/contexts/contextProvider";
import CustomChakraProvider from "@/providers/CustomChakraUi";

function App({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <link rel="icon" href="/movie.png" type="image/png" />
        <title>Watch Movies</title>
      </Head>
      <CustomChakraProvider>
        <GlobalStyles />
        <AppProvider>
          <Component {...pageProps} />
        </AppProvider>
      </CustomChakraProvider>
    </>
  );
}

export default App;
