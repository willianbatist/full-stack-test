import React from "react";
import { useRouter } from "next/router";
import { ContainerButtons, RegisterStyled } from "./register.style";
import { Button, Input, InputGroup, InputRightElement } from "@chakra-ui/react";
import { ViewIcon, ViewOffIcon } from "@chakra-ui/icons";
import { ContainerInput, ContainerInputs } from "../index.style";
import Footer from "@/components/footer/Footer";
import useRegisterForm from "@/hooks/useRegisterForm";

const Register: React.FC = () => {
  const {
    name,
    setName,
    email,
    setEmail,
    password,
    setPassword,
    nameError,
    emailError,
    passwordError,
    generalError,
    errorRegister,
    show,
    handleClick,
    handleSubmit,
  } = useRegisterForm();

  const router = useRouter();

  return (
    <>
      <RegisterStyled>
        <form
          onSubmit={(e) => {
            e.preventDefault();
            handleSubmit();
          }}
        >
          <h3>Cria conta</h3>
          <ContainerInputs>
            <ContainerInput>
              <label htmlFor="name">Nome</label>
              <Input
                placeholder="Informe seu nome"
                type="text"
                id="name"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
              {nameError && <p>{nameError}</p>}
            </ContainerInput>
            <ContainerInput>
              <label htmlFor="email">Email</label>
              <Input
                placeholder="Informe seu email"
                type="email"
                id="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              {emailError && <p>{emailError}</p>}
            </ContainerInput>
            <ContainerInput>
              <label htmlFor="password">Senha</label>
              <InputGroup size="md">
                <Input
                  type={show ? "text" : "password"}
                  placeholder="Informe sua senha"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
                <InputRightElement width="4.5rem">
                  <Button h="1.75rem" size="sm" onClick={handleClick}>
                    {show ? <ViewOffIcon /> : <ViewIcon />}
                  </Button>
                </InputRightElement>
              </InputGroup>
              {passwordError && <p>{passwordError}</p>}
              {generalError && <p>{generalError}</p>}
              {errorRegister && <p>{errorRegister}</p>}
            </ContainerInput>
          </ContainerInputs>
          <ContainerButtons>
            <Button
              id="buttonClosed"
              type="button"
              onClick={() => router.push("/")}
            >
              Cancelar
            </Button>
            <Button type="submit">Próximo</Button>
          </ContainerButtons>
        </form>
      </RegisterStyled>
      <Footer />
    </>
  );
};

export default Register;
