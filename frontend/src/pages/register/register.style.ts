'use client';
import styled, { css } from 'styled-components';
import backgroundImage from '../../../public/images/cinema.jpg';

export const RegisterStyled = styled.div`
  width: 100vw;
  min-height: 100vh;
  display: flex;
  justify-content: center;
  &::before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: url(${backgroundImage.src}) no-repeat center center fixed;
    background-size: cover;
    filter: brightness(0.5); /* Escurece a imagem */
    z-index: -1; /* Coloca a imagem atrás do conteúdo */
  }

  &::after {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: rgba(0, 0, 0, 0.5); /* Adiciona uma camada escura sobre a imagem */
    z-index: -1;
  }

  form {
    margin-top: 8rem;
    width: 600px;
    height: 450px;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-evenly;
    background-color: rgb(30, 30, 34);
    color: white;
  }

  h3 {
    font-style: normal;
    font-weight: 700;
    font-size: 1.5rem;
  }

  button {
      background-color: #E96744;
      color: white;
    }

  button:hover {
    background-color: #eb927b;
  }

  #buttonClosed {
    background-color: white;
    color: rgb(30, 30, 34);
  }
`;

export const ContainerButtons = styled.div`
  width: 80%;
  display: flex;
  justify-content: space-around;
`;