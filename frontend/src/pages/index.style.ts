'use client';
import styled, { css } from 'styled-components';
import backgroundImage from '../../public/images/cinema.jpg';

export const LoginStyled = styled.div`
  width: 100vw;
  min-height: 100vh;
  display: flex;
  justify-content: center;
  &::before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: url(${backgroundImage.src}) no-repeat center center fixed;
    background-size: cover;
    filter: brightness(0.5); /* Escurece a imagem */
    z-index: -1; /* Coloca a imagem atrás do conteúdo */
  }

  &::after {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: rgba(0, 0, 0, 0.5); /* Adiciona uma camada escura sobre a imagem */
    z-index: -1;
  }

  form {
    margin-top: 8rem;
    width: 600px;
    height: 450px;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-evenly;
    background-color: rgb(30, 30, 34);
    color: white;

    button {
      background-color: #E96744;
      color: white;
    }

    button:hover {
      background-color: #eb927b;
    }
  }

  h3 {
    font-style: normal;
    font-weight: 700;
    font-size: 1.5rem;
  }

  a:hover {
    color: #E96744;
    text-decoration: underline;
    text-decoration-color: #E96744;
  }

  @media(min-height: 850px) {
    margin-top: 10rem;
  }
`;

export const ContainerInputs = styled.div`
  width: 80%;
`;

export const ContainerInput = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  padding: 10px;

  input {
    height: 40px;
    background-color: white;
    color: rgb(30, 30, 34);
  }

  button {
    background-color: white;
  }

  a {
    margin-top: 10px;
    text-align: right;
  }

  p {
      font-size: 13px;
      color: red;
    }
`;

export const MenuLinks = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-evenly;
  width: 80%;
`;