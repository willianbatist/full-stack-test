// pages/Login.tsx
import React, { useContext } from "react";
import { Button, Input, InputGroup, InputRightElement } from "@chakra-ui/react";
import Link from "next/link";
import { AppContext } from "@/contexts/contextProvider";
import {
  LoginStyled,
  ContainerInputs,
  ContainerInput,
  MenuLinks,
} from "./index.style";
import { ViewIcon, ViewOffIcon } from "@chakra-ui/icons";
import Footer from "@/components/footer/Footer";
import { useLoginForm } from "@/hooks/useLoginForm";

const Login: React.FC = () => {
  const { setToken } = useContext(AppContext);
  const {
    email,
    setEmail,
    password,
    setPassword,
    handleSubmit,
    emailError,
    passwordError,
    generalError,
    errorLogin,
  } = useLoginForm(setToken);
  const [show, setShow] = React.useState(false);

  const handleClick = () => setShow(!show);

  return (
    <>
      <LoginStyled>
        <form
          onSubmit={(e) => {
            e.preventDefault();
            handleSubmit();
          }}
        >
          <h3>Entrar</h3>
          <ContainerInputs>
            <ContainerInput>
              <label htmlFor="email">Email</label>
              <Input
                placeholder="Informe seu email"
                type="email"
                id="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              {emailError && <p>{emailError}</p>}
            </ContainerInput>
            <ContainerInput>
              <label htmlFor="password">Senha</label>
              <InputGroup size="md">
                <Input
                  type={show ? "text" : "password"}
                  placeholder="Informe sua senha"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
                <InputRightElement width="4.5rem">
                  <Button h="1.75rem" size="sm" onClick={handleClick}>
                    {show ? <ViewOffIcon /> : <ViewIcon />}
                  </Button>
                </InputRightElement>
              </InputGroup>
              {passwordError && <p>{passwordError}</p>}
              {generalError && <p>{generalError}</p>}
              {errorLogin && <p>{errorLogin}</p>}
              <Link href="/forgot-password">Esqueci minha senha</Link>
            </ContainerInput>
          </ContainerInputs>
          <Button type="submit">Entrar</Button>
          <MenuLinks>
            <div>
              <p>Não tem conta?</p>
            </div>
            <div>
              <Link href="/register">Crie uma conta</Link>
            </div>
          </MenuLinks>
        </form>
      </LoginStyled>
      <Footer />
    </>
  );
};

export default Login;
