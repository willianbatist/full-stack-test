/* eslint-disable react-hooks/exhaustive-deps */
import { useContext, useEffect, useState } from "react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Button,
  useDisclosure,
} from "@chakra-ui/react";
import axios from "axios";
import { AppContext } from "@/contexts/contextProvider";
import Pagination from "@/components/pagination/Pagination";
import { ContainerStyled, MovieCard, MoviesStyled } from "./movies.style";
import Footer from "@/components/footer/Footer";
import Header from "@/components/header/Header";
import { useRouter } from "next/router";
import { fetchUser } from "@/services/api";

interface Movie {
  [x: string]: any;
  id: number;
  title: string;
}

const Movies: React.FC = () => {
  const [movies, setMovies] = useState<Movie[]>([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const [user, setUser] = useState<any>(null);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const limit = 10;

  const { token } = useContext(AppContext);
  const router = useRouter();

  const fetchMovies = async (page: number, token: string) => {
    try {
      const response = await axios.get(
        `http://localhost:4000/movies?page=${page}&limit=10`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      setMovies(response.data.movies);
      const totalItems = response.data.count;
      setTotalPages(Math.ceil(totalItems / limit));
    } catch (error) {
      console.error("Erro ao buscar filmes:", error);
    }
  };

  const handleFetchUser = async (token: string) => {
    try {
      const data = await fetchUser(token);
      setUser(data);
    } catch (error) {
      router.push("/");
    }
  };

  useEffect(() => {
    handleFetchUser(token);
  }, [token]);

  useEffect(() => {
    fetchMovies(currentPage, token);
  }, [currentPage]);

  const handlePageChange = (page: number) => {
    setCurrentPage(page);
  };

  return (
    <>
      <>
        <Modal isOpen={isOpen} onClose={onClose}>
          <ModalOverlay />
          <ModalContent bg={"rgb(30, 30, 34)"} color={"white"}>
            <ModalHeader>Perfil</ModalHeader>
            <ModalCloseButton />
            <ModalBody>
              {user && (
                <p>
                  <span style={{ color: "#E96744", fontWeight: "bold" }}>
                    Nome:
                  </span>
                  {` ${user.name}`}
                </p>
              )}
              {user && (
                <p>
                  <span style={{ color: "#E96744", fontWeight: "bold" }}>
                    Email:
                  </span>
                  {` ${user.email}`}
                </p>
              )}
            </ModalBody>

            <ModalFooter>
              <Button bg={"white"} color={"#E96744"} mr={3} onClick={onClose}>
                Fechar
              </Button>
              <Button bg={"#E96744"} onClick={() => router.push("/")}>
                Sair
              </Button>
            </ModalFooter>
          </ModalContent>
        </Modal>
      </>
      <Header onOpen={onOpen} />
      <MoviesStyled>
        <h1>Filmes</h1>
        <ContainerStyled>
          {movies.map((movie) => (
            <MovieCard key={movie.id}>
              <div>
                <p>
                  <span>Título:</span>
                  {` ${movie.title}`}
                </p>
                <p>
                  <span>Data de lançamento:</span>
                  {` ${movie.release_date}`}
                </p>
              </div>
              <div>
                <p>
                  <span>Avaliação:</span>
                  {` ${movie.vote_average.toFixed(1)}/10`}
                </p>
                <p>
                  <span>Linguagem original:</span>
                  {` ${movie.original_language.toUpperCase()}`}
                </p>
              </div>
            </MovieCard>
          ))}
        </ContainerStyled>
        <Pagination
          currentPage={currentPage}
          totalPages={totalPages}
          onPageChange={handlePageChange}
        />
      </MoviesStyled>
      <Footer />
    </>
  );
};

export default Movies;
