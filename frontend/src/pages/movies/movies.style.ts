'use client';
import styled from 'styled-components';
import backgroundImage from '../../../public/images/cinema.jpg';

export const MoviesStyled = styled.div`
  width: 100vw;
  min-height: 100vh;
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  justify-content: space-evenly;
  &::before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: url(${backgroundImage.src}) no-repeat center center fixed;
    background-size: cover;
    filter: brightness(0.5); /* Escurece a imagem */
    z-index: -1; /* Coloca a imagem atrás do conteúdo */
  }

  &::after {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: rgba(0, 0, 0, 0.5); /* Adiciona uma camada escura sobre a imagem */
    z-index: -1;
  }

  h1 {
    color: white;
    font-size: 30px;
    font-weight: bold;
    letter-spacing: 4px;

    margin-top: 40px;
    margin-bottom: 20px;
  }
`;

export const ContainerStyled = styled.div`
`;

export const MovieCard = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  margin-top: 10px;
  margin-bottom: 10px;
  padding: 10px;
  color:  white;
  background-color: rgb(30, 30, 34);
  font-weight: bold;

  -webkit-box-shadow: 7px 4px 14px 0px rgba(0,0,0,0.75);
  -moz-box-shadow: 7px 4px 14px 0px rgba(0,0,0,0.75);
  box-shadow: 7px 4px 14px 0px rgba(0,0,0,0.75);

  span {
    color: #E96744;
  }
`;