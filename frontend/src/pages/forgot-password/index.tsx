import { Button } from "@chakra-ui/react";
import { useRouter } from "next/router";
import { ContainerInputs } from "../index.style";
import { ContainerButtons, RegisterStyled } from "../register/register.style";
import Footer from "@/components/footer/Footer";

const ForgotPassword: React.FC = () => {
  const router = useRouter();

  return (
    <>
      <RegisterStyled>
        <form>
          <h3>Verificação</h3>
          <ContainerInputs>
            <p style={{ textAlign: "center", color: "#E96744" }}>
              ESTÁ EM DESENVOLVIMENTO...
            </p>
          </ContainerInputs>
          <ContainerButtons>
            <Button
              id="buttonClosed"
              type="button"
              onClick={() => router.push("/")}
            >
              Cancelar
            </Button>
            <Button type="submit" isDisabled={true}>
              Próximo
            </Button>
          </ContainerButtons>
        </form>
      </RegisterStyled>
      <Footer />
    </>
  );
};

export default ForgotPassword;
