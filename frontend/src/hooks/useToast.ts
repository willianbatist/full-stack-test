import { useToast } from "@chakra-ui/react";

export const useToastHook = () => {
  const toast = useToast();

  const showToast = (
    title: string,
    description: string,
    status: "success" | "error" | "warning" | "info" = "success",
    duration = 9000
  ) => {
    toast({
      title,
      description,
      status,
      duration,
      isClosable: true,
    });
  };

  return { showToast };
};
