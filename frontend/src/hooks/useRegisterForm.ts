import { useState } from "react";
import { useRouter } from "next/router";
import { validateEmail } from "@/utils";
import { UserFormValues } from "@/types";
import { createUser } from "@/services/api";
import { useToastHook } from "@/hooks/useToast";

const useRegisterForm = () => {
  const { showToast } = useToastHook();
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [nameError, setNameError] = useState("");
  const [emailError, setEmailError] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const [generalError, setGeneralError] = useState("");
  const [errorRegister, setErrorRegister] = useState("");
  const [show, setShow] = useState(false);

  const router = useRouter();

  const handleClick = () => setShow(!show);

  const handleShowToast = (
    title: string,
    message: string,
    status: "success" | "error" | "warning" | "info" | undefined
  ) => {
    showToast(title, message, status, 9000);
  };

  const handleRegister = async ({ name, email, password }: UserFormValues) => {
    try {
      const data = await createUser(name, email, password);
      handleShowToast("Sucesso:", "Conta criada com sucesso.", "success");
      console.log("User create successful:", data);
      router.push("/");
    } catch (error: any) {
      if (error.message === "Conflict") {
        setErrorRegister("Já existe uma conta com o e-mail.");
      } else {
        handleShowToast("Erro:", "Tente novamente mais tarde.", "error");
      }
      console.error("Error:", error);
    }
  };

  const handleSubmit = () => {
    let isValid = true;

    setEmailError("");
    setPasswordError("");
    setGeneralError("");
    setErrorRegister("");

    if (!email || !password || !name) {
      setGeneralError("Todos os campos são obrigatórios.");
      isValid = false;
    }

    if (!validateEmail(email || "")) {
      setEmailError("Email inválido.");
      isValid = false;
    }

    if ((name || "").length < 3) {
      setNameError("Nome inválido.");
      isValid = false;
    }

    if ((password || "").length < 4) {
      setPasswordError("A senha deve ter pelo menos 4 dígitos.");
      isValid = false;
    }

    if (isValid) {
      handleRegister({ name, email, password });
    }
  };

  return {
    name,
    setName,
    email,
    setEmail,
    password,
    setPassword,
    nameError,
    emailError,
    passwordError,
    generalError,
    errorRegister,
    show,
    handleClick,
    handleSubmit,
  };
};

export default useRegisterForm;
