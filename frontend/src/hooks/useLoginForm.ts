import { useState } from "react";
import { login } from "../services/api";
import { useToastHook } from "./useToast";
import { validateEmail } from "../utils";
import { LoginFormValues } from "@/types";
import { useRouter } from "next/router";

export const useLoginForm = (setToken: (token: string) => void) => {
  const { showToast } = useToastHook();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [emailError, setEmailError] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const [generalError, setGeneralError] = useState("");
  const [errorLogin, setErrorLogin] = useState("");
  const router = useRouter();

  const handleShowToast = () => {
    showToast("Erro:", "Tente novamente mais tarde.", "error", 9000);
  };

  const handleLogin = async ({ email, password }: LoginFormValues) => {
    try {
      const data = await login(email, password);
      setToken(data.access_token);
      console.log("Login successful:", data.access_token);
      router.push("/movies");
    } catch (error: any) {
      if (error.message === "Unauthorized") {
        setErrorLogin("Senha ou email incorretos.");
      } else {
        handleShowToast();
      }
      console.error("Error:", error);
    }
  };

  const handleSubmit = () => {
    let isValid = true;

    setEmailError("");
    setPasswordError("");
    setGeneralError("");
    setErrorLogin("");

    if (!email || !password) {
      setGeneralError("Todos os campos são obrigatórios.");
      isValid = false;
    }

    if (!validateEmail(email || "")) {
      setEmailError("Email inválido.");
      isValid = false;
    }

    if ((password || "").length < 4) {
      setPasswordError("A senha deve ter pelo menos 4 dígitos.");
      isValid = false;
    }

    if (isValid) {
      handleLogin({ email, password });
    }
  };

  return {
    email,
    setEmail,
    password,
    setPassword,
    emailError,
    passwordError,
    generalError,
    errorLogin,
    handleSubmit,
  };
};
