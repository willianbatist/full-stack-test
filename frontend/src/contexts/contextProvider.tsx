"use client";
import React, { createContext, useState } from "react";
import { AppContextInterface, Props } from "../types/index";

export const AppContext = createContext<AppContextInterface>(
  {} as AppContextInterface
);

const AppProvider = ({ children }: Props) => {
  const [token, setToken] = useState("");

  const context = {
    token,
    setToken,
  };

  return <AppContext.Provider value={context}>{children}</AppContext.Provider>;
};

export default AppProvider;
