export interface AppContextInterface {
  token: string,
  setToken: React.Dispatch<React.SetStateAction<string>>,
}


export interface Props {
  children: React.ReactNode;
}

export interface LoginFormValues {
  email: string;
  password: string;
}

export interface UserFormValues {
  name: string;
  email: string;
  password: string;
}